let nilai = prompt("Masukkan nilai anda.")

if (nilai >= 80 && nilai <= 100) {
    alert("Luar biasa! Anda mendapat nilai A.")
} else if (nilai >= 60 && nilai < 80) {
    alert("Bagus! Anda mendapat nilai B.")
} else if (nilai >= 40 && nilai < 60) {
    alert("Sudah baik. Anda mendapat nilai C")
} else if (nilai >= 20 && nilai < 40) {
    alert("Yaah! Anda mendapat nilai D.")
} else {
    alert("Astaga! Anda mendapat nilai E.")
}