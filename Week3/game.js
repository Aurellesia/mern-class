const ronde = 3;
let skorSatu = 0,
    skorDua = 0;

var button = document.getElementById("btn");
button.addEventListener('click', play)

function play() {
    game();
    winner();
    skorSatu = 0;
    skorDua = 0;
}

const bilanganRandom = (min, max) => {
    return Math.floor((Math.random() * (max - min)) + min);
}

const skor = () => {
    confirm(`Skor pemain satu : ${skorSatu} \nSkor pemain dua : ${skorDua}`)
}

const game = () => {
    for (let i = 1; i <= ronde; i++) {
        let bilRandom = bilanganRandom(1, 4);
        let nilaiSatu = parseFloat(prompt('Pemain 1 masukkan angka : '));
        let nilaiDua = parseFloat(prompt('Pemain 2 masukkan angka : '));

        if (!Number.isInteger(nilaiSatu) || !Number.isInteger(nilaiDua)) {
            alert("Masukkan angka!");
            skor();
            let choose = confirm("Ulangi?");
            if (choose) {
                i--;
                continue;
            } else {
                break;
            }
        } else {
            if (nilaiSatu < 1 || nilaiDua < 1) {
                alert("Tidak boleh menebak kurang dari 1!");
                skor();
                let choose = confirm("Ulangi?");
                if (choose) {
                    i--;
                    continue;
                } else {
                    break;
                }
            } else if (nilaiSatu > 3 || nilaiDua > 3) {
                alert("Tidak boleh menebak lebih dari 3!");
                skor();
                let choose = confirm("Ulangi?");
                if (choose) {
                    i--;
                    continue;
                } else {
                    break;
                }
            } else if (nilaiSatu == nilaiDua) {
                alert("Tidak boleh menebak angka yang sama!");
                skor();
                let choose = confirm("Ulangi?");
                if (choose) {
                    i--;
                    continue;
                } else {
                    break;
                }
            } else {
                if (nilaiSatu != bilRandom && nilaiDua != bilRandom) {
                    alert(`Angka yang dicari adalah ${bilRandom} \nTidak ada yang benar. Hasil SERI.`);
                    skor();
                    if (i + 1 <= ronde) {
                        let rond = confirm(`Ronde ${i+1} ?`);
                        if (rond) {
                            continue;
                        } else {
                            break;
                        }
                    } else {
                        break
                    }

                } else if (nilaiSatu == bilRandom) {
                    confirm(`Angka yang dicari adalah ${bilRandom} \nPemain 1 MENANG`);
                    skorSatu++;
                    skor();
                    if (i + 1 <= ronde) {
                        let rond = confirm(`Ronde ${i+1} ?`);
                        if (rond) {
                            continue;
                        } else {
                            break;
                        }
                    } else {
                        break
                    }
                } else if (nilaiDua == bilRandom) {
                    confirm(`Angka yang dicari adalah ${bilRandom} \nPemain 2 MENANG`);
                    skorDua++;
                    skor();
                    if (i + 1 <= ronde) {
                        let rond = confirm(`Ronde ${i+1} ?`);
                        if (rond) {
                            continue;
                        } else {
                            break;
                        }
                    } else {
                        break
                    }
                }
            }
        }
    }
}

const winner = () => {
    if (skorSatu == skorDua) {
        confirm("Tidak ada yang menang atau kalah. Hasilnya seri.");
    } else if (skorSatu > skorDua) {
        confirm("Selamat! Pemain 1 memenangkan permainan!");
    } else {
        confirm("Selamat! Pemain 2 memenangkan permainan!")
    }
}