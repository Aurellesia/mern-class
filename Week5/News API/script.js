// Search
const button = document.querySelector('.search-button');
button.addEventListener('click', function() {
    const searchInput = document.querySelector('.search-input');
    fetch(
            'https://newsapi.org/v2/everything?q=' +
            searchInput.value +
            '&language=en&apiKey=ace39ffa62e34c919e72c14b2fa3083f',
        )
        .then((response) => response.json())
        .then((response) => showNews(response))
        .catch((err) => console.log(err));
});

// Home
fetch(
        'https://newsapi.org/v2/top-headlines?country=us&category=general&language=en&apiKey=ace39ffa62e34c919e72c14b2fa3083f',
    )
    .then((response) => response.json())
    .then((response) => showNews(response))
    .catch((err) => console.log(err));

// Link entertaiment
const link = document.querySelectorAll('.category-link a');
link.forEach((l) => {
    l.addEventListener('click', function(e) {
        e.preventDefault();
        let val = this.dataset.value;
        fetch(
                `https://newsapi.org/v2/top-headlines?category=${val}&language=en&apiKey=ace39ffa62e34c919e72c14b2fa3083f`,
            )
            .then((response) => response.json())
            .then((response) => showNews(response))
            .catch((err) => console.log(err));
    });
});

// Function show
function showNews(response) {
    let news = response.articles;
    let paper = '';
    news.forEach(
        (news) =>
        (paper += `
    <div class="news-left col-10 border-right d-flex h-50 p-4">
        <div class="col-4 d-flex justify-content-center">
            <img src="${news.urlToImage}" class="image-fluid img-news">
        </div>
        <div class="news-body mx-4">
            <h3 class="news-title">${news.title}</h3>
            <p class="news-text">${news.description}</p>
            <a href="${news.url}" class="continue-reading"><strong>Continue reading...</strong></a>
        </div>
    </div>

    <div class="news-right col d-flex flex-column justify-content-center">
        <p class="source">Source : ${news.source.name}</p>
        <p class="published">Published at : <br>${date(news.publishedAt)}</p>
    </div>`),
    );
    const cardContainer = document.querySelector('.news-container');
    cardContainer.innerHTML = paper;
}


// function convert date string to date time
function date(dateString) {
    let newDate = new Date(dateString).toUTCString();
    let arr = newDate.split(" ");
    arr.pop();
    let newArr = [];
    arr.forEach(item => {
        if (item === 'Mon' || item === 'Sun' || item === 'Tue' || item === 'Wed' || item === 'Thu' || item === 'Fri' || item === 'Sat') {
            item = `${item},`;
        }
        newArr.push(item);
    });
    let newString = newArr.join(" ");
    return newString;
}