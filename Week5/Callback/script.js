const requestAjax = (url, success, error) => {
    const tableContainer = document.getElementsByTagName("TBODY")[0];

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 1) {
            console.log("Loading")
            tableContainer.innerHTML = "Loading ... ";
        }
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                console.log("Berhasil")
                success(xhr.response);
                tableContainer.innerHTML = success(xhr.response);
            } else {
                console.log("Gagal")
                error();
            }
        }
    }
    xhr.open('GET', url, true);
    xhr.send();
}

const error = () => {
    console.log('Failed');
}

const success = data => {
    const dataUser = JSON.parse(data);
    const table = dataUser.map(item => {
        return `
                <tr>
                    <td>${item.id}</td>
                    <td>${item.name}</td>
                    <td>${item.username}</td>
                    <td>${item.email}</td>
                    <td>${item.address.street} ${item.address.suite} ${item.address.city}</td>
                    <td>${item.company.name}</td>
                </tr>`
    }).join('');
    return table;
}

requestAjax('https://jsonplaceholder.typicode.com/users', success, error);