export class Table {
    constructor(items) {
        this.items = items;
    }

    tableHeader(columns) {
            const header = `
              <thead>
                <tr>
                  ${columns.map(column => { 
                    return `<th scope="col">${column}</th>`;
                  }).join("")} 
                </tr>
              </thead>`;
    return header;
  }

  tableBody(datas) {
          const body = `
      <tbody>
        ${datas.map((data) => {
          return `<tr>
            ${data.map((dt) => {
              return `<td>${dt}</td>`;
            }).join("")}
          </tr>`;
        }).join("")}
      </tbody>`;
    return body;
  }

  render(element) {
    const table = `
    <div class="table-container">
      <table class="table">
          ${this.tableHeader(this.items.columns)}
          ${this.tableBody(this.items.data)}
      </table>
      </div>`;
    element.innerHTML = table;
  }
}






export class Card{
  constructor(items){
    this.items = items;
  }

  createBody(data){
    return `<div class="card">
              <div class="icon">
                  <img src="icons/right-quotes-symbol-white.png" alt="quote" class="quote-icon">
              </div>
              <p class="card-text ">${data.text}</p>
              <figure class="image">
                  <img class="img-profile" src="${data.image}" alt="">
              </figure>
              <h5 class="card-name ">${data.name}</h5>
              <h6 class="card-job">${data.job}</h6>
            </div>`
  }

  render(element){
    const card = `
      <div class="container">
        ${this.items.map((value) => {
          return this.createBody(value);
        }).join('')}
        </div>`;
      
    element.innerHTML = card;
  }
}