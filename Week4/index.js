import { Table, Card } from "./libs.js"

const tableContainer = document.getElementById("table");
const dataTable = new Table({
    columns: ['Nama Depan', 'Nama Belakang', 'Email', 'Alamat', 'Jurusan'],
    data: [
        ['Aurellesia', 'Warsito', 'aurellesia19@gmail.com', 'Malang', 'TI'],
        ['Rifatul', 'Chusnul', 'rifatul@gmail.com', 'Malang', 'Pend Bio'],
        ['Sekar', 'Sungkono', 'sekar@gmail.com', 'Malang', 'Psikologi'],
        ['Ferika', 'Anggraini', 'ferika@gmail.com', 'Bogor', 'Biologi']
    ]
})

const cardContainer = document.getElementById("card");
// const data = ;
const dataCard = new Card(
    [{
            text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia,\
    molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum \
    numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium \
    optio, eaque rerum!',
            image: 'img/pict1.jpg',
            name: 'John Wick',
            job: 'Graphic Designer',
        },
        {
            text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia,\
    molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum \
    numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium \
    optio, eaque rerum!',
            image: 'img/pict2.jpg',
            name: 'Ethan Stone',
            job: 'Web Developer',
        }, {
            text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia,\
    molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum \
    numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium \
    optio, eaque rerum!',
            image: 'img/pict3.jpg',
            name: 'Travis Cooper',
            job: 'Content Writer',
        },
    ]);

dataTable.render(tableContainer);
dataCard.render(cardContainer);